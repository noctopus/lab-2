package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t);
	}

	public String toString() {
		String begin = Float.toString(this.getValue());
		String end = " F";
		return begin + end;
	}

	@Override
	public Temperature toCelsius() {
		float temp = this.getValue();
		temp = (temp - 32) * 5 / 9;
		return new Fahrenheit(temp);
	}

	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit(this.getValue());
	}

	@Override
	public Temperature toKelvin() {
		float temp = this.getValue();
		temp = (temp - 32) * 5 / 9 + 273;
		return new Kelvin(temp);
	}
}
