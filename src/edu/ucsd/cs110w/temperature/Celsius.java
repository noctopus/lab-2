package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
	}

	public String toString() {
		String begin = Float.toString(this.getValue());
		String end = " C";
		return begin + end;
	}

	@Override
	public Temperature toCelsius() {
		return new Celsius(this.getValue());
	}

	@Override
	public Temperature toFahrenheit() {
		float temp = this.getValue();
		temp = (temp * 9 / 5) + 32;
		return new Fahrenheit(temp);
	}

	@Override
	public Temperature toKelvin() {
		float temp = this.getValue();
		temp = temp + 273;
		return new Kelvin(temp);
	}
}
