/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (cs110wcd): write class javadoc
 * 
 * @author cs110wcd
 * 
 */
public class Kelvin extends Temperature {

	public Kelvin(float t) {
		super(t);
	}

	public String toString() {
		String begin = Float.toString(this.getValue());
		String end = " K";
		return begin + end;
	}

	@Override
	public Temperature toCelsius() {
		float temp = this.getValue();
		temp = temp - 273;
		return new Celsius(temp);
	}

	@Override
	public Temperature toFahrenheit() {
		return this.toCelsius().toFahrenheit();
	}

	@Override
	public Temperature toKelvin() {
		return new Kelvin(this.getValue());
	}

}
